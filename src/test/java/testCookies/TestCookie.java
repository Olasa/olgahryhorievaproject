package testCookies;

import baseTest.BaseTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import page_object.BasePage;
import page_object.LoginFormPage;
import page_object.MainPage;
import page_object.SecurePage;

public class TestCookie extends BaseTest {

    LoginFormPage loginFormPage;
    MainPage mainPage;
    String userName = "tomsmith";
    String password = "SuperSecretPassword!";
    String expectedTooltipText = "You logged into a secure area!";
    SecurePage securePage;
/*    String expectedTitle = "Secure Area";
    String expectedErrorTooltipText = "You must login to view the secure area!";*/


    @BeforeClass
    public void beforeClass() {
        openUrl("https://the-internet.herokuapp.com");
        mainPage = new MainPage(driver);
        loginFormPage = new LoginFormPage(driver);
        securePage = new SecurePage(driver);
    }

    @Test

    public void cookieTest() {
        mainPage.clickOnBtnFromPage("login");
        loginFormPage.login(userName, password)
                .checkSuccessTooltip(expectedTooltipText);
        BasePage.ShowAllCookies();
        System.out.println();
        BasePage.CreateMyCookie();
        System.out.println();
        BasePage.DeleteMyCookie();
        BasePage.DeleteAllCookies();
        BasePage.RefreshCookies();
    }

}
