package alertsTest;

import baseTest.BaseTest;
import enum_from_page.AlertButtons;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page_object.AlertPage;
import page_object.MainPage;

public class AlertTest extends BaseTest {

    AlertPage alertsPage;

    @BeforeMethod
    public void beforeClass() {

        openUrl("https://the-internet.herokuapp.com");
       alertsPage = new AlertPage(driver);
    }

    @Test
    public void alertTest() {

        new MainPage(driver).clickOnBtnFromPage("javascript_alerts");
        alertsPage.clickOnButton(AlertButtons.ALERT.getTextOnButton());
        Assert.assertEquals(alertsPage.switchToAlertAndGetText(true), "I am a JS Alert");
        Assert.assertEquals(alertsPage.getResultText(), "You successfully clicked an alert");

    }
}



