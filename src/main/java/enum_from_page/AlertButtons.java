package enum_from_page;

public enum AlertButtons {
    ALERT("JS Alert"),
    CONFIRM("JS Confirm"),
    PROMPT("JS Prompt");

    private final String textOnButton;

    AlertButtons(String textOnButton) {
        this.textOnButton = textOnButton;
    }

    public String getTextOnButton() {
        return textOnButton;
    }

}
