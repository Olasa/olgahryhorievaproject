package page_object;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.Set;

public class BasePage {
    protected static WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }

    //----------------1й метод выводит на экран все куки showAllcookies
    public static void ShowAllCookies() {
        Set<Cookie> cookies = driver.manage().getCookies();
        for (Cookie cookie : cookies) {
            System.out.println(cookie.getName() + " " + cookie.getValue() + cookie.getExpiry());
        }
    }
    //-------------------2й метод create my cookies ----------------
    public static void CreateMyCookie() {
        Cookie myCookie = new Cookie("MyCookie", "MyValue");
        driver.manage().addCookie(myCookie);
        ShowAllCookies();
    }
    //--------------------3й метод удаляет мою куку--------------
    public static void DeleteMyCookie() {
        driver.manage().deleteCookieNamed("MyCookie");
        ShowAllCookies();
    }
    //--------------------4й удаляет все куки---------------------
    public static void DeleteAllCookies() {
        driver.manage().deleteAllCookies();
    }
    //-----------5й метод refresh------------------------------
    public static void RefreshCookies() {
        driver.navigate().refresh();
    }

}


