package baseTest;

import org.driver.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {

    protected WebDriver driver = null;

    @BeforeTest
    public void setUpBrowser() {
        /*ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver();*/
        driver = WebDriverFactory.initDriver();
        driver.manage().window().maximize();

    }
    @AfterTest
    public  void closeDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

    public void openUrl(String url) {
        driver.navigate().to(url);
    }
}
