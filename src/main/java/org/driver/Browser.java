package org.driver;

public enum Browser {

    CHROME,
    FIREFOX,
    EDGE
}
