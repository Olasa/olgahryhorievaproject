package alertsTest;

import baseTest.BaseTest;
import enum_from_page.AlertButtons;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import page_object.AlertPage;
import page_object.MainPage;

public class ConfirmTest extends BaseTest {

    AlertPage alertsPage;

    @BeforeMethod
    public void beforeMethod() {
        openUrl("https://the-internet.herokuapp.com");
        alertsPage = new AlertPage(driver);
    }
    @DataProvider(name = "testParam")
    Object[] dataProvider() {
        return new Object[][] {
                {true, "You clicked: Ok"},
                {false, "You clicked: Cancel"}
        };
    }

    @Test(dataProvider = "testParam")
    public void alertTest(boolean switcher, String expectedText) {

        new MainPage(driver).clickOnBtnFromPage("javascript_alerts");
        alertsPage.clickOnButton(AlertButtons.CONFIRM.getTextOnButton());
        Assert.assertEquals(alertsPage.switchToAlertAndGetText(switcher), "I am a JS Confirm");
        Assert.assertEquals(alertsPage.getResultText(), expectedText);
    }
}