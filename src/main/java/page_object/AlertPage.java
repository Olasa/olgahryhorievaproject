package page_object;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AlertPage extends BasePage {


    public AlertPage(WebDriver driver) {
        super(driver);
    }

    private By alertsBtn(String nameButton){
        return By.xpath("//button[contains(text(),'" + nameButton + "')]");

    }

    private By resultText(){
        return By.cssSelector("#result");

     ///=================================================================

    }
    public void clickOnButton(String button) {
        System.out.println(button);
        driver.findElement(alertsBtn(button)).click();

    }

    public String switchToAlertAndGetText(boolean confirm, String... messages) {
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        if (messages.length > 0) {
            alert.sendKeys(messages[0]);
        }
        if (confirm) {
            alert.accept();
        } else {
            alert.dismiss();
        }
        return alertText;
    }

    public String getResultText() {
        return driver.findElement(resultText()).getText();
    }

}

