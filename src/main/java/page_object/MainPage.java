package page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class MainPage extends BasePage{
        public MainPage(WebDriver driver) {
            super(driver);
        }
/*@FindBy(xpath = "//a[@href='/javascript_alerts']")
    private WebDriver alertsBtn;*/

        private By btnFromPage(String nameButton) {
            return By.xpath("//a[@href='/" + nameButton + "']");
        }

        //======================================================================

        public void clickOnBtnFromPage(String nameButton) {
            driver.findElement(btnFromPage(nameButton)).click();
        }
    }
